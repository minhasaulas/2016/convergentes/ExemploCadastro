package br.edu.up;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class Executor {

	public static void main(String[] args) throws Exception {
		
		Connection conn = getMySqlConnection();
		//CallableStatement call = conn.prepareCall("SELECT * FROM exemplo.listar;");
		CallableStatement call = conn.prepareCall("CALL `sakila`.`listar`(3);");

		ResultSet rs = call.executeQuery();
		
		while(rs.next()){
			System.out.println("Col1: " + rs.getString(1));
			System.out.println("Col2: " + rs.getString(2));
			System.out.println("Col3: " + rs.getString(3));
			System.out.println("----------");
		}
		conn.close();
	}
	
	public static Connection getMySqlConnection() throws Exception {
		String driver = "org.gjt.mm.mysql.Driver";
		String url = "jdbc:mysql://localhost:3306/sakila";
		String username = "root";
		String password = "master";

		Class.forName(driver);
		Connection conn = DriverManager.getConnection(url, username, password);
		return conn;
	}

	public static Connection getHSQLConnection() throws Exception {
		Class.forName("org.hsqldb.jdbcDriver");
		System.out.println("Driver Loaded.");
		String url = "jdbc:hsqldb:data/exemplo";
		return DriverManager.getConnection(url, "root", "master");
	}

	public static Connection getOracleConnection() throws Exception {
		String driver = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@localhost:1521:exemplo";
		String username = "root";
		String password = "master";

		Class.forName(driver);
		Connection conn = DriverManager.getConnection(url, username, password);
		return conn;
	}
}