package br.edu.up;

import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Cadastro {
	
	public static void main(String[] args) {

		Produto p1 = new Produto();
		p1.setNome("P1");
		p1.setPreco(new BigDecimal("65.01"));
		
		Produto p2 = new Produto();
		p2.setNome("P2");
		p2.setPreco(new BigDecimal("34.10"));
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sistemas");
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		em.persist(p1);
		em.persist(p2);
		em.getTransaction().commit();
		
		System.out.println("Produto 1: " + p1.getId());
		System.out.println("Produto 2: " + p2.getId());
		
		em.close();
	}
}